# LIS 4369 -Extensible Enterprise Solutions

## Keith Eikevik

### Assignment 2 Requirements:

*Two points:*

1. Backward-engineer (using .NET Core) console application
2. Chapter questions

#### README.md file should include the following items:

* Screenshot console application running valid option
* Screenshot console application running invalid option

#### Assignment Screenshots:

*Screenshot of console application running valid option*:

![Screenshot of hwapp running](img/valid.png)

*Screenshot of console application running invalid option*:

![Screenshot of aspnetcoreapp application running](img/invalid.png)

