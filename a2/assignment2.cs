﻿using System;

namespace ConsoleApplication
{
    public class assignment2
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("/////////////////////////////////////////////////////////////");

            Console.WriteLine("A2: Simple Calculator");
            Console.WriteLine("Note: Program does not perform data validation.");
            Console.WriteLine("Author: Keith C. Eikevik");

            DayOfWeek today = DateTime.Today.DayOfWeek;
			DateTime now = DateTime.Now;
			Console.WriteLine("Now: " + today + ", "+ now);
			

            Console.WriteLine("/////////////////////////////////////////////////////////////");

            double number1, number2;
            int choice1 = 0;
            bool validInput = false;

            Console.Write("\nNum1: ");
            number1 = Double.Parse(Console.ReadLine());
            Console.Write("Num2: ");
            number2 = Double.Parse(Console.ReadLine());

            Console.WriteLine("\n1- Addition");
            Console.WriteLine("2- Subtraction");
            Console.WriteLine("3- Multiplication");
            Console.WriteLine("4- Division");

            Console.Write("\nChoose a mathmatical operation: ");
            choice1 = Int32.Parse(Console.ReadLine());  
            switch(choice1)
            {
            case 1:
                Console.WriteLine("\nChoice: " + choice1 + " - Addition");
                Console.WriteLine("*** Result of addition operation: ***");
                Console.WriteLine(number1 + number2);
                break;
            case 2:
                Console.WriteLine("\nChoice: " + choice1 + " - Subtraction");
                Console.WriteLine("*** Result aof subtracion operation: ***");
                Console.WriteLine(number1 - number2);
                break;
            case 3:
                Console.WriteLine("\nChoice: " + choice1 + " - Multiplication");
                Console.WriteLine("*** Result of Multiplication operation: ***");
                Console.WriteLine(number1 * number2);
                break;
            case 4:
                Console.WriteLine("\nChoice: " + choice1 + " - Division");
                Console.WriteLine("\n*** Result of division operation: ***");
                if (number2 == 0)
                {
                    Console.WriteLine("Division by 0 is not allowed"); //can't divide by 0, DUHHH
                }
                else
                {
                    Console.WriteLine(number1 / number2);
                }
                break;
            default:
                Console.Write("Invalid input. Please enter a number from 1 to 4: ");
                break;
            }

    Console.WriteLine("\nPress any key to exit");
    Console.ReadKey();

            
        }
    }
}
