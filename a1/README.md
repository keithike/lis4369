# LIS 4369 -Extensible Enterprise Solutions

## Keith Eikevik

### Assignment 1 Requirements:

*Four points:*

1. Distrubuted version Control with Git and Bitbucket
2. Development Installations
3. Chapter questions (ch 1 and 2)
4. Bitbucket repo links

#### README.md file should include the following items:

* Screenshot of hwapp application running
* Screenshot of aspnetcoreapp application running
* Git Commands with short descriptions

#### Git commands with short descriptions:

1. git init: Creates an empty Git repository or reinitialize an existing one.
2. git status: Shows the working tree status.
3. git add: Add file contents to the index.
4. git commit: Records changes to the repository.
5. git push: Updates remote refs along with associated objects.
6. git pull: Fetchs from and integrates with another repository or a local branch.
7. git clone: Create a working copy of a local repository.

#### Assignment Screenshots:

*Screenshot of hwapp running*:

![Screenshot of hwapp running](img/hwapp.png)

*Screenshot of aspnetcoreapp application running*:

![Screenshot of aspnetcoreapp application running](img/aspnet.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/keithike/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/keithike/myteamquotes/ "My Team Quotes Tutorial")
