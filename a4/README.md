# LIS 4369 -Extensible Enterprise Solutions

## Keith Eikevik

### Assignment 4 Requirements:

*Two points:*

1. Backward-engineer (using .NET Core) console application (Future Value Calculator)
2. Chapter questions

#### README.md file should include the following items:

* Screenshot console application running valid option

#### Assignment Screenshots:

*Screenshot of console application running valid option*:

![Screenshot of inheritance running](img/inher1.png)
![Screenshot of inheritance running](img/inher2.png)


