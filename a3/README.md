# LIS 4369 -Extensible Enterprise Solutions

## Keith Eikevik

### Assignment 3 Requirements:

*Two points:*

1. Backward-engineer (using .NET Core) console application (Future Value Calculator)
2. Chapter questions

#### README.md file should include the following items:

* Screenshot console application running valid option
* Screenshot console application running invalid option

#### Assignment Screenshots:

*Screenshot of console application running valid option*:

![Screenshot of hwapp running](img/vin.png)


