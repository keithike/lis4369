﻿using System;

namespace a5
{
    public class Program
    {


    public static void Main(string[] args)
    {
      string requirements =
      @"
    *******************************************************************************************************************************
      Program Requirements:
      Using Classes - Inheritance, while also usibg Overloading and Overriding 
      Author: Keith Eikevik

	A) Create Vehicle class
        1) Create Two private data members:
            a. milesTraveled (float)
            b. gallonsUsed (float)
        2) Create four properties
			a. Manufacturer (string)
			b. Make (string)
			c. Model (string)
			d. MPG (float)
        3) Create two setter/accessor methods.
			a. SetMiles
			b. SetGallons
		4) Create four getter/accessor methods.
			a. GetMiles
			b. GetGallons
			c. GetObjectinfo()
			d.GetObjectinfo(arg) (overload method)
		5) Create two constructors.
			a. default constructor (accepts no args)
			b. parameterized constructor with default parameter values that accepts three arguments
	B) Create car class
		1) create one private member:
			a. style (string)
		2) create one property
			a. style (string)
		3) Create two constructors
			a. default constructor (accepts no args)
			b. parameterized constructor with default parameter values that accepts four arguments
		4) Create one getter/accessor method
			a. GetObjectinfo(arg) (overridden method): accepts string arg to be used as a seperater for display purposes.
	C) Instantiate three Vehicle objects:
		1) one from default constructor. display data member values
		2) two from parameterized constructor (passing arguments to its constructor parameters). display data member values.
	D) Instantiate one car object:
		1) one from parameterized constructor (passing arguments to its constructor parameters). display data member values.
	E) Muat include data validation on numeric input,

	F) Allow user to press any key to return back to command line.
    *****************************************************************************************************************************
       ";
      Console.WriteLine(requirements);

      DateTime dat = DateTime.Now;
      Console.WriteLine("  Now: {0:d}, {0:T}    \n\n\n", dat);       //display current date and time

      Console.WriteLine("\nvehicle1 - (Instantiating new object from default constructor)");
      Vehicle vehicle1 = new Vehicle();

      Console.WriteLine(vehicle1.GetObjectInfo());

      //call parameterized constructor
      Console.WriteLine("\nvehicle2 (user input): Call parameterized base constructor (accepts arguments):");

      //initalize arguments
      string p_manufacturer = "";
      string p_make = "";
      string p_model = "";
      float p_miles = 0.0f;
      float p_gallons = 0.0f;
      string p_separator = "";

      //user input
      Console.Write("Manufacturer (alpha): ");
      p_manufacturer = Console.ReadLine();

      Console.Write("Make (alpha): ");
      p_make = Console.ReadLine();

      Console.Write("Model (alpha): ");
      p_model = Console.ReadLine();

      Console.Write("Miles Driven (float): ");
      while (!float.TryParse(Console.ReadLine(), out p_miles))
      {
      	Console.Write("miles must be numeric: ");
      }

      Console.Write("Gallons used (float): ");
      while (!float.TryParse(Console.ReadLine(), out p_gallons))
      {
      	Console.Write("Gallons must be numeric: ");
      }

      Console.WriteLine("\nvehicle2 - (Instantiating new object, passing only 1st arg)\n" + "(demos why use constructor with default parameter values):");
      Vehicle vehicle2 = new Vehicle(p_manufacturer);

      Console.WriteLine(vehicle2.GetObjectInfo());

      Console.WriteLine("\nUser input: vehicle2 - passing arg to overload getObjectinfo(arg):");
      Console.Write("Delimiter (, : ;): ");

      p_separator = Console.ReadLine();

      Console.WriteLine(vehicle2.GetObjectInfo(p_separator)); //overload same scope

      Console.WriteLine("\nVehicle3 - (Instantiating new object, passing all vehicle args):");
      Vehicle vehicle3 = new Vehicle(p_manufacturer, p_make, p_model);

      vehicle3.SetGallons(p_gallons);
      vehicle3.SetMiles(p_miles);

      Console.WriteLine("\nUser input: vehicle3 - passing arg to overloaded GetObjectinfo(arg)");
      Console.Write("Delimiter (, : ;): ");
	  p_separator = Console.ReadLine();
      Console.WriteLine(vehicle3.GetObjectInfo(p_separator));

      Console.WriteLine("\nDemonstrating Polymorphism new derved object):");

      //initalize arguments
      string p_style = "";

      //cap user input
      Console.Write("Manufacturer (alpha): ");
      p_manufacturer = Console.ReadLine();

      Console.Write("Make (alpha): ");
      p_make = Console.ReadLine();

      Console.Write("Model (alpha): ");
      p_model = Console.ReadLine();

      Console.Write("Miles Driven: ");
      while (!float.TryParse(Console.ReadLine(), out p_gallons))
      {
      	Console.Write("Miles must be numeric: ");
      }

      Console.Write("Gallons used: ");
      while (!float.TryParse(Console.ReadLine(), out p_gallons))
      {
      	Console.Write("Gallons must be numeric: ");
      }

      //2dr, 4dr, convert, coupe, hatch
      Console.Write("Style (alphanumeric): ");
      p_style = Console.ReadLine();

      Console.WriteLine("\ncar1 - (Instantiating new dervied object passing all args):");
      Car car1 = new Car(p_manufacturer, p_make, p_model, p_style);

      //car class has access to vehicles setgallons and setmiles methods
      car1.SetGallons(p_gallons);
      car1.SetMiles(p_miles);

      Console.WriteLine("\nUser input car1 - passing arf to overriden getObjectinfo(arg):");
      Console.Write("Delimiter (, : ;): ");
      p_separator = Console.ReadLine();
      Console.WriteLine(car1.GetObjectInfo(p_separator));

      Console.WriteLine("\n\nPress any key to exit");

      Console.ReadKey();


    } // end of main




    }
}
