# LIS 4369 -Extensible Enterprise Solutions

## Keith Eikevik

### Assignment 5 Requirements:

*Two points:*

1. Backward-engineer (using .NET Core) console application (Car Calculator)
2. Chapter questions

#### README.md file should include the following items:

* Screenshot console application running valid option

#### Assignment Screenshots:

*Screenshot of console application running valid option*:

![Screenshot of overridding running](img/a5car.png)


