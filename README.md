# LIS 4369 -Extensible Enterprise Solutions

## Keith Eikevik

### LIS 4369 Requirements:

*Homework Links*

1. [A1 README.md](a1/README.md "My A1 README.md file")

    * Install .NET Core
    * Create hwapp application
    * Create aspnetcoreapp application
    * Provide screenshots of Installations
    * Complete Bitbucket tutorials
    * Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")

    * Backward-engineer (using .NET Core) console application
    * Comeplete chapter questions

3. [A3 README.md](a3/README.md "My A3 README.md file")

    * Backward-engineer (using .NET Core) console application Future Value Calculator
    * Complete chapter questions

4. [P1 README.md](p1/README.md "My P1 README.md file")

    * Backward-engineer (using .NET Core) console application Rooms Size Calculator
    * Complete chapter questions

4. [A4 README.md](a4/README.md "My A4 README.md file")
    
    * Backward-engineer (using .NET Core) console application Inheritance
    * Complete chapter questions

5. [A5 README.md](a5/README.md "My A5 README.md file")
    
    * Create Vehicle (base/parent/super) class
    * Create Car (derived/child/sub) class

6. [P2 README.md](p2/README.md "My P2 README.md file")
    
    * Use LINQ to create program