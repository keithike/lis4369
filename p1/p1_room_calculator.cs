using System; //imports all libraries
namespace roomClass
{
  public class p1_room_Calculator //declared class
  {
    public static void Main(String[] args)
    {
      DateTime dat = DateTime.Now;

      string requirements =
      @"***********************************************************************************
      Information and Requirements:
      Title: Project 1 - Room Calculator
      Author: Keith Eikevik
        1) Create Room class
        2) Create following fields (aka properties or data members)
          a. private string type 
          b. private double length 
          c. private double width
          d. private double height 
        3) Create two constructors
          a. Default constructor
          b. parameterized constructor that accepts four arguments (four fields above)
        4) Create the following mutator (aka setter) methods:
          a. SetType
          b. SetLength
          c. SetWidth
          d. SetHeight
        5) Create the followign accessor (aka getter) methods:
          a. GetType
          b. GetLength
          c. GetWidth
          d. GetHeight
          e. GetArea
          f. GetVolume
        6) Must include the following functionalilty:
          a. Display room size calculations in feet (as per below)
          b. Must include data validation
          c. Round to two decimal places
        7) Allow user to press any key to return back to command line.
      ************************************************************************";
      Console.WriteLine("^^^^^^^^Now: {0:d}, {0:T}^^^^^^^^^^^^", dat);
      Console.WriteLine(requirements);

      //program starts using 'room' class, here
      Room roomObject = new Room(); //declared and instanciates the object 'roomObject'
      Console.WriteLine("Room Type: " + roomObject.GetRoomType());
      Console.WriteLine("Room Length: " + roomObject.GetRoomLength());
      Console.WriteLine("Room Width: " + roomObject.GetRoomWidth());
      Console.WriteLine("Room Height: " + roomObject.GetRoomHeight());
      Console.WriteLine("Room Area: " + roomObject.GetRoomArea() + " sq ft");
      Console.WriteLine("Room Volume: " + roomObject.GetRoomVolume().ToString("F2") + " cu ft"); //output double with 2 decimals (ToString("F2"))
      Console.WriteLine("Room Volume: " + (roomObject.GetRoomVolume()/27).ToString("F2") + " cu yd"); //output double with 2 decimals (ToString("F2"))

      Console.WriteLine("\n2- Modify the default room constructor's data member values:" );
      Console.WriteLine("Use setter/getter methods");
      //Initialize some variables to output
      double rLength = 0.0;
      double rWidth = 0.0;
      double rHeight = 0.0;

      Console.Write("Room type: ");
      roomObject.SetType(Console.ReadLine());

      Console.Write("Room length: ");
      while(!double.TryParse(Console.ReadLine(), out rLength))
      {
        Console.Write("Please input a numberic length: ");
      }

      Console.Write("Room width: ");
      while(!double.TryParse(Console.ReadLine(), out rWidth))
      {
        Console.Write("Please input a numberic width: ");
      }

      Console.Write("Room height: ");
      while(!double.TryParse(Console.ReadLine(), out rHeight))
      {
        Console.Write("Please input a numberic height: ");
      }

      //setting the constructor variables equal to the new inputted
      roomObject.SetLength(rLength);
      roomObject.SetWidth(rWidth);
      roomObject.SetHeight(rHeight);

      Console.WriteLine("\n3- Display " + roomObject.GetRoomType() + " room object's NEW data member values");
      Console.WriteLine("Room Type: " + roomObject.GetRoomType());
      Console.WriteLine("Room Length: " + roomObject.GetRoomLength());
      Console.WriteLine("Room Width: " + roomObject.GetRoomWidth());
      Console.WriteLine("Room Height: " + roomObject.GetRoomHeight());
      Console.WriteLine("Room Area: " + roomObject.GetRoomArea() + " sq ft");
      Console.WriteLine("Room Volume: " + roomObject.GetRoomVolume().ToString("F2") + " cu ft"); //output double with 2 decimals (ToString("F2"))
      Console.WriteLine("Room Volume: " + (roomObject.GetRoomVolume()/27).ToString("F2") + " cu yd"); //output double with 2 decimals (ToString("F2"))

      Room roomObject2 = new Room("string", 10.0, 10.0, 10.0);  //creating new room for 'roomObject2'
      Console.Write("Room type: ");
      roomObject2.SetType(Console.ReadLine());

      Console.Write("Room length: ");
      while(!double.TryParse(Console.ReadLine(), out rLength))
      {
        Console.Write("Please input a numberic length: ");
      }

      Console.Write("Room width: ");
      while(!double.TryParse(Console.ReadLine(), out rWidth))
      {
        Console.Write("Please input a numberic width: ");
      }

      Console.Write("Room height: ");
      while(!double.TryParse(Console.ReadLine(), out rHeight))
      {
        Console.Write("Please input a numberic height: ");
      }

      //setting the new constructor variables equal to the new inputted
      roomObject2.SetLength(rLength);
      roomObject2.SetWidth(rWidth);
      roomObject2.SetHeight(rHeight);

      Console.WriteLine("\n5- Creating " + roomObject2.GetRoomType() + " object from parameterized constructor:");
      Console.WriteLine("Room Type: " + roomObject2.GetRoomType());
      Console.WriteLine("Room Length: " + roomObject2.GetRoomLength());
      Console.WriteLine("Room Width: " + roomObject2.GetRoomWidth());
      Console.WriteLine("Room Height: " + roomObject2.GetRoomHeight());
      Console.WriteLine("Room Area: " + roomObject2.GetRoomArea() + " sq ft");
      Console.WriteLine("Room Volume: " + roomObject2.GetRoomVolume().ToString("F2") + " cu ft"); //output double with 2 decimals (ToString("F2"))
      Console.WriteLine("Room Volume: " + (roomObject2.GetRoomVolume()/27).ToString("F2") + " cu yd"); //output double with 2 decimals (ToString("F2"))

      Console.WriteLine("\nPress any key to exit");
      Console.ReadKey();
    }
  }


}
