# LIS 4369 -Extensible Enterprise Solutions

## Keith Eikevik

### Project 1 Requirements:

*Two points:*

1. Backward-engineer (using .NET Core) console application (Future Value Calculator)
2. Chapter questions

#### README.md file should include the following items:

* Screenshot console application running valid option

#### Assignment Screenshots:

*Screenshot of console application running valid option*:

![Screenshot of p1 running](img/p1.1.png)
![Screenshot of p1 running](img/p1.2.png)

